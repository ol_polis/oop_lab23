#include"TLongint.h"


using namespace std;


int choice();
void prog_interface();
void list_of_commands();

int main() {
	list_of_commands();
	prog_interface();
	system("pause");
	return 0;
}

void list_of_commands() {
	cout << "Choose one of the methods 1-4 for multiplication, 5 - for searching an inverse " << endl;
	cout << "number, 6 - for division, 7-9 - for testing simplicity." << endl;
	cout << "If you want to see list of commands enter 10. If you want to exit enter 0." << endl;
	cout << "1. Karatsuba " << '\n';
	cout << "2. Toom-Cook" << '\n';
	cout << "3. Shonhage" << '\n';
	cout << "4. Strassen" << '\n';
	cout << "5. Cook method for searching an inverse element" << '\n';
	cout << "6. Cook division" << '\n';
	cout << "7. Lehmann test" << '\n';
	cout << "8. Rabin-Miller test" << '\n';
	cout << "9. Solovay-Strassen test" << '\n';
	cout << "10. List of commands." << '\n';
	cout << endl;
}


void prog_interface() {
	int f = choice();
	double start = 0, end = 0, time = 0;
	while (f != 0) {
		switch (f) {
		case 1: {
			cout << "Karatsuba multiplication" << endl;
			cout << "Enter x, please. ";
			TLongint * x = new TLongint;
			cin >> *x;
			cout << "Enter y, please. ";
			TLongint * y = new TLongint;
			cin >> *y;
			TLongint t;
			start = clock();
			t = (*x).karatsuba((*y));
			end = clock();
			cout << t;
			time = (end - start) / CLOCKS_PER_SEC;
			cout << "Running time: " << time << endl;
			delete x;
			delete y;
			break;
		}
		case 2: {
			cout << "Toom-Cook multiplication." << endl;
			cout << "Enter x, please. ";
			TLongint * x = new TLongint;
			cin >> *x;
			cout << "Enter y, please. ";
			TLongint * y = new TLongint;
			cin >> *y;
			TLongint t;
			start = clock();
			t = (*x).toomCook((*y));
			end = clock();
			cout << t;
			time = (end - start) / CLOCKS_PER_SEC;
			cout << "Running time: " << time << endl;
			delete x;
			delete y;
			break;
		}
		case 3: {
			cout << "Shenhage multiplication." << endl;
			cout << "Enter x, please. ";
			TLongint * x = new TLongint;
			cin >> *x;
			cout << "Enter y, please. ";
			TLongint * y = new TLongint;
			cin >> *y;
			TLongint t;
			start = clock();
			t = (*x).shonhage(*y);
			end = clock();
			cout << t;
			time = (end - start) / CLOCKS_PER_SEC;
			cout << "Running time: " << time << endl;
			delete x;
			delete y;
			break;
		}
		case 4: {
			cout << "Shenhage-Strassen multiplication." << endl;
			cout << "Enter x, please. ";
			TLongint * x = new TLongint;
			cin >> *x;
			cout << "Enter y, please. ";
			TLongint * y = new TLongint;
			cin >> *y;
			TLongint t;
			start = clock();
			t = (*x).shonhageStrassen(*y);
			end = clock();
			cout << t;
			time = (end - start) / CLOCKS_PER_SEC;
			cout << "Running time: " << time << endl;
			delete x;
			delete y;
			break;
		}
		case 5: {
			cout << "Cook algorithm for finding an inverse element." << endl;
			cout << "Enter x, please. ";
			TLongint * x = new TLongint;
			cin >> *x;
			TLongint t;
			start = clock();
			t = (*x).cookInverse();
			//cout << t;
			end = clock();
			t.printInverse((*x).getSize());
			time = (end - start) / CLOCKS_PER_SEC;
			cout << "Running time: " << time << endl;
			delete x;
			break;
		}
		case 6: {
			cout << "Cook division." << endl;
			cout << "Enter x, please. ";
			TLongint * x = new TLongint;
			cin >> *x;
			cout << "Enter y, please. ";
			TLongint * y = new TLongint;
			cin >> *y;
			TLongint t, p;
			start = clock();
			t = (*y).cookInverse();
			p = (*x).cookDiv(t, (*y).getSize());
			end = clock();
			cout << p;
			time = (end - start) / CLOCKS_PER_SEC;
			cout << "Running time: " << time << endl;
			delete x;
			delete y;
			break;
		}
		case 7: {
			cout << "Lehmann test for simplicity." << endl;
			cout << "Enter x, please. ";
			TLongint * x = new TLongint;
			cin >> *x;
			int d;
			start = clock();
			d = (*x).lehmann();
			end = clock();
			cout << d << endl;
			time = (end - start) / CLOCKS_PER_SEC;
			cout << "Running time " << time << endl;
			delete x;
			break;
		}
		case 8: {
			cout << "Rabin-Miller test for simplicity." << endl;
			cout << "Enter x, please. ";
			TLongint * x = new TLongint;
			cin >> *x;
			int d;
			start = clock();
			d = (*x).rabinMiller();
			end = clock();
			cout << d << endl;
			time = (end - start) / CLOCKS_PER_SEC;
			cout << "Running time: " << time << endl;
			delete x;
			break;
		}
		case 9: {
			cout << "Solovay-Strassen test for simplicity." << endl;
			cout << "Enter x, please. ";
			TLongint * x = new TLongint;
			cin >> *x;
			int d;
			start = clock();
			d = (*x).solovayStrassen();
			end = clock();
			cout << d << endl;
			time = (end - start) / CLOCKS_PER_SEC;
			cout << "Running time: " << time << endl;
			delete x;
			break;
		}
		case 10: {
			list_of_commands();
		}
		}
		f = choice();
	}
}

int choice() {
	int f;
	cout << "Please, choose a method. ";
	cin >> f;
	while ((f > 11) || (f < 0)) {
		cout << "Please, enter a number between 1 and 9." << endl;
		cin >> f;
	}
	return f;
}
