#pragma once
#include <SDKDDKVer.h>
#include <tchar.h>
# include <stdio.h>
# include <iostream>
# include <cmath>
# include <stdlib.h>
# include <ctime>
# include <vector>
# include <string>

# define MINUS -1
# define PLUS 1
# define MAX_ITER 10
# define Sqr(X) X*X

using namespace std;

class TLongint {
private:
	vector<int> digits;
	int sign;
public:
	TLongint(char * str);
	TLongint();
	TLongint(const  TLongint & x);
	~TLongint();
	friend ostream& operator<< (ostream& stream, const TLongint &);
	friend istream& operator >> (istream& stream, TLongint &);
	TLongint & operator= (const TLongint &);
	TLongint& operator= (char *s);
	TLongint operator* (const TLongint &);
	TLongint operator* (const int &);
	TLongint operator+ (const TLongint &);
	TLongint operator+ (const int &);
	TLongint operator- (const TLongint &);
	TLongint operator- (const int &);
	TLongint operator/ (const TLongint &);
	TLongint operator/ (const int &);
	TLongint operator% (const TLongint &);
	TLongint operator% (const int &);
	bool operator <(const TLongint &);
	bool operator <=(const TLongint &);
	bool operator > (const TLongint &);
	bool operator >=(const TLongint &);
	bool operator ==(const TLongint &);
	bool lessAbsVal(const TLongint &); // returns true, if |(*this)| < |argument|
	int getSize() { return digits.size(); } // returns number of digits
	TLongint symbJacobi(TLongint); // Jacobi symbol       ????????????????????????????????????????????
	TLongint gcd(TLongint); // greates common dividor       ���
	TLongint exp_mod(const TLongint &, const TLongint &); // modular exponentation ???????????????????????
	TLongint convertDecBi(); // converts a number from decimal into binary 
	TLongint convertBiDec(); // converts a number from binary into decimal
	TLongint exp(const TLongint &); // exponentation
	TLongint gcd_extended(TLongint, TLongint &, TLongint &); // extended Euclid algorithm
	void normalize(); // do carries
	void pbZero(int); // adds zeros at the end of a number
	void delBackZero(); // deletes zeros at the end of a number
	void printInverse(const int &);
	TLongint karatsuba(const TLongint &);
	TLongint karatsRecur(const TLongint &, int);
	TLongint toomCook(const TLongint &);
	TLongint shonhageStrassen(const TLongint &);
	TLongint shonhage(const TLongint &);
	TLongint cookInverse();
	TLongint cookDiv(const TLongint &, const int &);
	bool lehmann();
	bool rabinMiller();
	bool solovayStrassen();
};



void FFT(vector <double> &, bool inverse);
